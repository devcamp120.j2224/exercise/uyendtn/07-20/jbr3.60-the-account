package com.devcamp.jbr360.accountrestapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr360.accountrestapi.model.Account;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("accounts")
    public ArrayList<Account> accountList() {

        Account account1 = new Account("ACC1", "OHMM", 50000);
        Account account2 = new Account("ACC2", "XTRA", 80000);
        Account account3 = new Account("ACC3", "PILOTA", 80000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());
        System.out.println(account3.toString());

        ArrayList<Account> accList = new ArrayList<Account>();
        accList.add(account1);
        accList.add(account2);
        accList.add(account3);
        return accList;
    }
}
